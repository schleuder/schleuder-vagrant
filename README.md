schleuder-vagrant
=================

A vagrant box for schleuder.

The idea is to easily get a simple VM with an up and running installation of
schleuder on a supported distribution. Including schleuder-api-daemon, as well
as on CentOS with an up & running [schleuder-web](https://0xacab.org/schleuder/schleuder-web/) installation.

After a `vagrant up` you can browse to [https://127.0.0.1:8443/](https://0xacab.org/schleuder/schleuder-web/) and work with schleuder.

It will use the [puppet module](https://0xacab.org/schleuder/puppet-schleuder) to provision a fully functional schleuder installation,
as well as a working MTA with schleuder being fully hooked into.

You are able to choose supported distributions (atm. Debian stretch & CentOS 7) within the
configuration. See more about how to configure a box more below.

The MTA is configured in a way that it accepts **any** email. It delivers any email
to a configured schleuder list and all other emails are delivered to a maildir in
`/var/mail/mails/$recipient`, where $recipient matches the emails recipient.
This gives you a closed email system, where you are able to send emails back-and-forth
through schleuder lists, as well as inspect any messages sent by schleuder.

You can define as many schleuderlist as you want in the configuration and it will
create these for you.

Requirements
------------

* [Vagrant](https://www.vagrantup.com/) (See your distribution's guide on how to get it running)
* [Virtualbox](https://www.virtualbox.org/) (with which it is tested, other providers should technically work as well)
* [vbguest plugin](https://github.com/dotless-de/vagrant-vbguest) (`vagrant plugin install vagrant-vbguest` - if using VirtualBox)

Usage
-----

* Make sure you have all the requirements
* `git clone https://0xacab.org/schleuder/schleuder-vagrant`
* `cd schleuder-vagrant`
* `vagrant up`

Done!

If you are adding a list after the box has been setup, you can just run `vagrant provision`
to rerun the necessary puppet parts again.

To start from scratch or you'd like to try out another distribution, first `vagrant destroy`,
edit the configuration then start from the beginning.

To update the puppet modules, remove the file `scripts/puppet/Puppetfile.lock`, this
will re-trigger a librarian-puppet run on the next `vagrant provision`.

To use the [schleuder-latest Repository](https://copr.fedorainfracloud.org/coprs/schleuder/schleuder-latest/) run Vagrant with SCHLEUDER_LATEST set to 1: `SCHLEUDER_LATEST=1 vagrant up`

Portforwarding
--------------

The following ports are forwarded by default from the local machine to the box, making
it easy to interact with with the installed software:

* local machine -> vagrant box
* 4443 -> 4443 (schleuder api-daemon)
* 2525 -> 25 (MTA)
* 8080 -> 80
* 8443 -> 443

Configuration
-------------

You can tune the current configuration of the box by providing a `config.yaml` file.

This file is used as a config file for the [Vagrantfile](Vagrantfile), as well as a hiera data source
for puppet.

The sample configuration file [config.yaml.sample](config.yaml.sample) contains all
possible and default settings. Please refer to that file for the different options.

Lists can be created, by defining them in the `schleuder::lists` hash. It has the
following format:

```
schleuder::lists:
    listaddress@domain:
      admin: initialadminemail@domain
    anotherlist@anotherdomain:
      admin: initialadminemail@domain
      admin_publickey: |
        AN OPTIONAL ARMORED PGP PUBLIC KEY
        OF initialadminemail@domain
```

Architecure
-----------

The architecture of this vagrant box is pretty simple:

* `config.yaml` or the defaults define the image to be used by vagrant, by default it takes the
  official [centos/7 image](https://app.vagrantup.com/centos/boxes/7). A selected image
  should be ready to be configured further, through its specific `setup.sh`.
* provisioning of the box happens through [scripts/provision.sh](scripts/provision.sh) which takes the
  used image-name as an argument. It delegates a first set of setup tasks to
  `scripts/$image-name/setup.sh`
* `scripts/$image-name/setup.sh` is supposed to configure the box so that a) we are
  able to run puppet (>= 4.8), b) the distro's package manager can install the schleuder
  packages and c) we have self-signed certificate in the distributions common location,
  also containing a file with the fingerpint. See [scripts/debian/stretch64/setup.sh](scripts/debian/stretch64/setup.sh)
  for an example.
* Once the distribution specific things are done, [scripts/puppet/apply.sh](scripts/puppet/apply.sh) takes
  over and a) installs [librarian-puppet](http://librarian-puppet.com/) to b) fetch all the
  necessary puppet modules and c) invokes a `puppet apply` on [scripts/puppet/site.pp](scripts/puppet/site.pp).
* [scripts/puppet/site.pp](scripts/puppet/site.pp) setups haveged (to speedup key generation) and the local MTA,
  copies the pre-generated self-signed certs to schleuders location, hands over the
  right parameter to the [schleuder class](https://0xacab.org/schleuder/puppet-schleuder/blob/master/manifests/init.pp) and on CentOS
  it also installs and configures you a working [schleuder-web](https://0xacab.org/schleuder/schleuder-web/) installation.


ToDo
----

* Add support for postfix.
* Make it possible to get schleuder running from latest HEAD, e.g. by running all the
  components based on checked out repositories in `dev/` to ease development / debugging.
* schleuder-web for Debian
* More distributions

Contributing
------------

Please see [CONTRIBUTING.md](CONTRIBUTING.md).


Code of Conduct
---------------

We adopted a code of conduct. Please read [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md).


License
-------

GNU GPL 3.0. Please see [LICENSE.txt](LICENSE.txt).
