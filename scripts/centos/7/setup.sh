#!/bin/bash

# setup dependencies
if [ ! -f /etc/profile.d/puppet-agent.sh ]; then
  rpm -Uvh http://yum.puppetlabs.com/puppet/puppet-release-el-7.noarch.rpm
  yum install -y puppet
fi

set -e

source /etc/profile.d/puppet-agent.sh
if [ "${SCHLEUDER_LATEST}" == "1" ]; then
  [ -f '/etc/yum.repos.d/schleuder-schleuder-latest-epel-7.repo' ] || curl -s -o /etc/yum.repos.d/schleuder-schleuder-latest-epel-7.repo https://copr.fedorainfracloud.org/coprs/schleuder/schleuder-latest/repo/epel-7/schleuder-schleuder-laste-epel-7.repo
else
  [ -f '/etc/yum.repos.d/schleuder-schleuder-epel-7.repo' ] || curl -s -o /etc/yum.repos.d/schleuder-schleuder-epel-7.repo https://copr.fedorainfracloud.org/coprs/schleuder/schleuder/repo/epel-7/schleuder-schleuder-epel-7.repo
fi

puppet apply -e "
package{
  ['epel-release','rubygems']:
    ensure => installed
}
"

if [ ! -f /etc/pki/tls/private/localhost.key ]; then

  /usr/bin/openssl genrsa -rand /proc/apm:/proc/cpuinfo:/proc/dma:/proc/filesystems:/proc/interrupts:/proc/ioports:/proc/pci:/proc/rtc:/proc/uptime 2048 > /etc/pki/tls/private/localhost.key 2> /dev/null

  FQDN=`hostname`
  if [ "x${FQDN}" = "x" -o ${#FQDN} -gt 59 ]; then
    FQDN=$(facter fqdn)
  fi

  cat << EOF | /usr/bin/openssl req -new -key /etc/pki/tls/private/localhost.key \
         -x509 -sha256 -days 365 -set_serial $RANDOM -extensions v3_req \
         -out /etc/pki/tls/certs/localhost.crt 2>/dev/null
--
SomeState
SomeCity
SomeOrganization
SomeOrganizationalUnit
${FQDN}
root@${FQDN}
EOF
fi

# fp for cert
[ -f /etc/pki/tls/certs/localhost.fp ] || (openssl x509 -sha256 -fingerprint -in /etc/pki/tls/certs/localhost.crt | head -n 1 | awk -F= '{ print $2 }' | tr "[A-Z]" "[a-z]" | sed 's/://g' > /etc/pki/tls/certs/localhost.fp)
