#!/bin/bash

base=$(dirname $(readlink -f $0))

if [[ -z $1 || ! -x $base/$1/setup.sh ]]; then
  echo "Requires a valid image to start! The image $1 is not supported so far!"
  exit 1
fi

# setup prereqs for image type
#
# we expect this script to setup everything
# meaning:
#
#   * we can install librarian-puppet using the gem provider
#   * all dependencies are here for the puppet module to take over

set -e

$base/$1/setup.sh

set +e

[ -f /etc/profile.d/puppet-agent.sh ] && source /etc/profile.d/puppet-agent.sh

# now we let puppet do the rest
$base/puppet/apply.sh
