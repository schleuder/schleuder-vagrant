#!/bin/bash

# setup dependencies
if [ ! -f /usr/bin/puppet ]; then
  apt-get install -y puppet
fi

set -e

# Currently, only schleuder-cli is available via backports, so we need the current testing suite 'buster' as well
update=0

if [ ! -f /etc/apt/sources.list.d/buster.list ]; then
  echo "deb http://deb.debian.org/debian buster main" > /etc/apt/sources.list.d/buster.list
  update=1
fi
if [ ! -f /etc/apt/sources.list.d/stretch-backports.list ]; then
  echo "deb http://deb.debian.org/debian stretch-backports main" > /etc/apt/sources.list.d/stretch-backports.list
  update=1
fi

# Set up APT pinning using a very low priority to ensure we only pull in specific packages out of 'buster'
if [ ! -f /etc/apt/preferences.d/buster-pinning.pref ]; then
  echo "Package: *
Pin: release a=buster
Pin-Priority: 100" > /etc/apt/preferences.d/buster-pinning.pref
  update=1
fi

if [ ! -f /etc/apt/preferences.d/stretch-backports-pinning.pref ]; then
  echo "Package: *
Pin: release a=stretch-backports
Pin-Priority: 150" > /etc/apt/preferences.d/stretch-backports-pinning.pref
  update=1
fi

# Update APT sources to refresh the package cache
if [ $update -gt 0 ]; then
  apt-get update
fi

if [ ! -d /etc/ssl/localcerts ]; then
  mkdir -p /etc/ssl/localcerts
  openssl req -new -x509 -days 365 -nodes -out /etc/ssl/localcerts/localhost.pem -keyout /etc/ssl/localcerts/localhost.key -subj "/CN=$(facter ipaddress)"
  chmod 600 /etc/ssl/localcerts/localhost*
fi

# fp for cert
[ -f /etc/ssl/localcerts/localhost.fp ] || (openssl x509 -sha256 -fingerprint -in /etc/ssl/localcerts/localhost.pem --noout | awk -F= '{ print $2 }' | tr "[A-Z]" "[a-z]" | sed 's/://g' > /etc/ssl/localcerts/localhost.fp)
