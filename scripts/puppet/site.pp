# setup schleuder
Exec { path => '/sbin:/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin', }

class vagrant_schleuder(
  $mailer = 'exim'
){
  # pre-reqs
  if $facts['osfamily'] == 'Debian' {
    $tls_key = '/etc/ssl/localcerts/localhost.key'
    $tls_cert = '/etc/ssl/localcerts/localhost.pem'
    $fingerprint_file = '/etc/ssl/localcerts/localhost.fp'
  } elsif $facts['osfamily'] == 'RedHat' {
    $tls_key = '/etc/pki/tls/private/localhost.key'
    $tls_cert = '/etc/pki/tls/certs/localhost.crt'
    $fingerprint_file = '/etc/pki/tls/certs/localhost.fp'
  } else {
    fail("Osfamily ${facts['osfamily']} not yet supported!")
  }

  if versioncmp($facts['facterversion'],'3.0') >= 0 {
    $host_ip = $facts['networking']['ip']
  } else {
    $host_ip = $facts['ipaddress']
  }

  include haveged
  Service['haveged'] -> Package['schleuder']
  # use the pre-generated certs (mainly for schleuder web
  exec{
    "cp ${tls_key} /etc/schleuder/schleuder-private-key.pem":
      creates => '/etc/schleuder/schleuder-private-key.pem',
      require => Package['schleuder'],
      before  => File['/etc/schleuder/schleuder.yml'];
    "cp ${tls_cert} /etc/schleuder/schleuder-certificate.pem":
      creates => '/etc/schleuder/schleuder-certificate.pem',
      require => Package['schleuder'],
      before  => File['/etc/schleuder/schleuder.yml'];
  }

  # by default we don't really have any keys here
  Schleuder::List{
    admin_publickey => ''
  }
  class{'schleuder':
    api_host    => $host_ip,
    cli_api_key => sha1("${fqdn_rand(1204,'cli')}"),
    web_api_key => sha1("${fqdn_rand(1024,'web')}"),
  }

  if $mailer == 'exim' {
    if $facts['osfamily'] == 'RedHat' {
      $pkg_name = 'exim'
      $exim_file_path  = '/etc/exim/exim.conf'
      $exim_service_name = 'exim'
      $exim_group = 'mail'
      $mailx_pkg = 'mailx'
      $schleuder_lists_file = '/etc/exim/schleuderlists'
    } elsif $facts['osfamily'] == 'Debian' {
      $pkg_name = 'exim4-base'
      $exim_file_path  = '/etc/exim4/exim4.conf'
      $exim_service_name = 'exim4'
      $exim_group = 'Debian-exim'
      $mailx_pkg = 'bsd-mailx'
      $schleuder_lists_file = '/etc/exim4/schleuderlists'
    } else {
      fail("Mailer configuration for osfamily ${facts['osfamily']} not yet supported!")
    }
    service{'postfix':
      ensure => stopped,
      enable => false,
    } -> package{'postfix':
      ensure => absent,
    } -> package{$pkg_name:
      ensure => installed,
    } -> file{
      $schleuder_lists_file:
        ensure => file,
        mode   => '0644';
      $exim_file_path:
        content => template('/vagrant/scripts/puppet/templates/exim.conf.erb');
      [$tls_key,$tls_cert]:
        mode    => '0640',
        group   => $exim_group;
    } ~> service{$exim_service_name:
      ensure => running,
      enable => true,
    } -> package{$mailx_pkg:
      ensure => installed,
    }
    # refresh the schleuderlists file if anything is done with a list
    Schleuder_list<||> ~> exec{"schleuder-cli lists list > $schleuder_lists_file":
      refreshonly => true,
      environment => ['HOME=/root'],
      subscribe   => File['/var/lib/schleuder/adminkeys'],
      before      => File[$schleuder_lists_file]
    }
    # make sure exim is running, before sending out any mails
    Service[$exim_service_name] -> Exec<| tag == 'schleuder-cli-send-list-key-to-subscriptions' |>

  } elsif $mailer == 'postfix' {
    if $facts['osfamily'] == 'RedHat' {
      $pkg_name = 'postfix'
      $postfix_main_file_path = '/etc/postfix/main.cf'
      $postfix_master_file_path = '/etc/postfix/master.cf'
      $postfix_service_name = 'postfix'
      $postfix_group = 'mail'
      $schleuder_lists_file = '/etc/postfix/schleuderlists'
    } else {
      fail("Mailer configuration for osfamily ${facts['osfamily']} not yet supported!")
    }
    service{'exim':
      ensure => stopped,
      enable => false,
    } -> package{'exim':
      ensure => absent,
    } -> package{$pkg_name:
      ensure => installed,
    } -> file{
      $schleuder_lists_file:
        ensure => file,
        mode   => '0644';
      $postfix_main_file_path:
        content => template('/vagrant/scripts/puppet/templates/postfix_main.cf.erb');
      $postfix_master_file_path:
        content => template('/vagrant/scripts/puppet/templates/postfix_master.cf.erb');
      '/etc/postfix/transport_schleuder':
        content => template('/vagrant/scripts/puppet/templates/postfix_transport_schleuder.erb');
      [$tls_key,$tls_cert]:
        mode    => '0640',
        group   => $postfix_group;
    } ~> service{$postfix_service_name:
      ensure => running,
      enable => true,
    }
    # refresh the schleuderlists file if anything is done with a list
    Schleuder_list<||> ~> exec{"schleuder-cli lists list > $schleuder_lists_file":
      refreshonly => true,
      environment => ['HOME=/root'],
      subscribe   => File['/var/lib/schleuder/adminkeys'],
      before      => File[$schleuder_lists_file]
    }
    # make sure postfix is running, before sending out any mails
    Service[$postfix_service_name] -> Exec<| tag == 'schleuder-cli-send-list-key-to-subscriptions' |>

  } else {
    fail("Not (yet) able to run mailer: ${mailer}")
  }

  if $facts['osfamily'] == 'RedHat' {
    # get the fingerprint in the first run
    $tls_fingerprint = chomp(pick(getvar('::schleuder_tls_fingerprint'),file($fingerprint_file)))
    package{'mod_ssl':
      ensure => installed,
    } -> class{'schleuder::web':
      api_key             => '<%= ENV["SCHLEUDER_API_KEY"] %>',
      api_tls_fingerprint => '<%= ENV["SCHLEUDER_TLS_FINGERPRINT"] %>',
      api_host            => '<%= ENV["SCHLEUDER_API_HOST"] %>',
      web_hostname        => '<%= ENV["SCHLEUDER_WEB_HOSTNAME"] %>',
    } -> file{'/etc/httpd/conf.d/vhost.conf':
      content => template('/vagrant/scripts/puppet/templates/vhost.conf.erb'),
      notify  => Service['httpd'],
    } -> selboolean{'httpd_can_network_connect':
        persistent => true,
        value      => 'on',
    } -> service{'httpd':
      ensure       => running,
      enable       => true,
    }
  }
}

include vagrant_schleuder
