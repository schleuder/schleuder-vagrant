#!/bin/bash -e

# remove the lockfile to rerun the modules update
rootdir=$(dirname $(readlink -f $0))
if [ ! -f $rootdir/Puppetfile.lock ]; then
  puppet apply -e "
  package{
    'librarian-puppet':
      ensure   => installed,
      provider => gem;
    'git':
      ensure   => installed;
  }"


  [ -d $rootdir/modules ] || mkdir $rootdir/modules


  cd $rootdir
  LIBRARIAN_PUPPET_TMP=/tmp/ /usr/local/bin/librarian-puppet install
fi

puppet apply --hiera_config $rootdir/hiera.yaml --modulepath $rootdir/modules/ $rootdir/site.pp
